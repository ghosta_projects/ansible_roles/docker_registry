Private docker-registry with basic auth and standalone sertificate
=========

Don't forget copy registry.crt file from docke-registry instance to clients to: /etc/docker/certs/d/"registry_host_name":"registry_port"/ca.crt. [More info](https://docs.docker.com/engine/security/certificates/)

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - docker_registry
